<!DOCTYPE html>
<html>
    <head>
        <title>Hyperbet</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="general.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
        <script>
            var userName = "<?=$_SESSION['userName'];?>"
            var userTag = "<?php if($userRank == 7) { echo "[Owner]";} elseif ($userRank == 3) { echo "[Mod]";} elseif ($userRank == 5) { echo "[Admin]";} else {}?>"
            var userPic = "<?=$userPicture?>"
            var userSteamID = "<?=$_SESSION['steamid']?>"
            var userBalance = "<?=$userBalance?>"
            var userbalance = "<?=$userBalance?>"
            var user64id = "<?=$_SESSION['steamid']?>"
            const socket = io('http://198.20.228.80:8002');
            socket.on('error', function(e){
                console.log('error', e);
            });
            socket.on("connect",function(){
                console.log("socket connected");
            });
            socket.on('userBetBlack', function(listBlackBet){
                $('#betListBlack').append($('<li><img src="'+ betPicLink + '"class="betListingPicture"><h2 class="bettingUserName">' + betTag + betName + '</h2><h1 class="bettingAmount">' + betAmtValue + '</h1>'))
            });
            socket.on('userBetGreen', function(listGreenBet){
                $('#betListGreen').append($('<li><img src="'+ betPicLink + '"class="betListingPicture"><h2 class="bettingUserName">' + betTag + betName + '</h2><h1 class="bettingAmount">' + betAmtValue + '</h1>'));
            });
            socket.on('userBetRed', function(listRedBet){
                $('#betListRed').append($('<li><img src="'+ betPicLink + '"class="betListingPicture"><h2 class="bettingUserName">' + betTag + betName + '</h2><h1 class="bettingAmount">' + betAmtValue + '</h1>'))
            });
            socket.on('userBetGold', function(listGoldBet){
                $('#betListGold').append($('<li><img src="'+ betPicLink + '"class="betListingPicture"><h2 class="bettingUserName">' + betTag + betName + '</h2><h1 class="bettingAmount">' + betAmtValue + '</h1>'))
            });
        </script>
    </head>
        <?php include "header.php"?>
        <?php include "chat.php"?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <div id="mainContent">
            <div id="countdownTimer"><h2>Next Round in: <?php ?></h2></div>
            <div id="stdroulette-container" class="">
                <div id="roulette-container-background"></div>
                <div id="roulette-indicator-id" class="roulette-indicator"></div>
                <div id="roulette-images">
                    <div id = "roulette-images-list-list">
                    <ul id="roulette-images-list">
                        <li class="gold"><h3 class="betColorListInsideText">17</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="gold"><h3 class="betColorListInsideText">17</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="gold"><h3 class="betColorListInsideText">17</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="gold"><h3 class="betColorListInsideText">17</h3></li>
                    </ul>
                    </div>
                </div>
            </div>
            <div id="rouletteHashInformation"><h3>Round #: Round Seed: (Secret: )</h3></div>
            <ul id="betValue">
                <li id="gameBalance"><h3>Balance: <?php if(isset($_SESSION['steamid'])) { echo "$balance1"; } else { echo "0"; }?></h3></li>
                <li><h3>Bet Amount:</h3></li>
                <li><input id="userBetAmountInNum" type="number" name="betAmount"></li>
                <li><ul id="betValueModifierBTNS">
                    <li><button id="halftimesbetBTN">1/2x</button></li>
                    <li><button id="twotimesbetBTN">2x</button></li>
                    <li><button id="maxbetBTN">All-in</button></li>
                    <li><button id="clearbetBTN">Clear</button></li>
                </ul></li>
            </ul>
            <ul id="betSelect2">
                <li><button id="redBetButton" name="redBet">Red</button><h2>Red Total:</h2></li>
                <li><button id="blackBetButton" name="blackBet">Black</button><h2>Black Total:</h2></li>
                <li><button id="greenBetButton" name="greenBet">Green</button><h2>Green Total:</h2></li>
                <li><button id="goldBetButton" name="goldBet">Gold</button><h2>Gold Total:</h2></li>
            </ul> 
            <ul id="allBetList2">
                <ul id="betListRed"></ul>
                <ul id="betListBlack"></ul>
                <ul id="betListGreen"></ul>
                <ul id="betListGold"></ul>
            </ul> 
        </div>
    </body>
    <script type="text/javascript" src="general.js"></script>
    <script type="text/javascript" src="betroulettescript.js"></script>
</html>