window.onload=function(){
    var welcomePopClose = document.getElementById('closeOut');
    welcomePopClose.addEventListener("click", closeout);
    var chatWindowClose = document.getElementById('chatWindowClose');
    chatWindowClose.addEventListener("click", chatClose);
    var chatWindowOpen = document.getElementById('chatWindowOpen');
    chatWindowOpen.addEventListener("click", chatOpen);
    var tosCloseButton = document.getElementById('tosCloseButton');
    tosCloseButton.addEventListener("click", closeTOS);
    var tosOpenButton = document.getElementById('tosOpenButton');
    tosOpenButton.addEventListener("click", openTOS);
    var provablyOpenButton = document.getElementById('provablyOpenButton');
    provablyOpenButton.addEventListener("click", openProvably);
    var provablyCloseButton = document.getElementById('provablyCloseButton');
    provablyCloseButton.addEventListener("click", closeProvably);
    var affiliateOpenButton = document.getElementById('affiliatesOpenButton');
    affiliateOpenButton.addEventListener("click", openAffiliates);
    var affiliateCloseButton = document.getElementById('affiliatePopupModalCloseBTN');
    affiliateCloseButton.addEventListener("click", closeAffiliates);
}
function closeout(){
    document.getElementById('welcomePop').style.display="none";
}

function chatClose(){ 
    document.getElementById('chatWindowContainer').style.display="none";
    document.getElementById('chatWindowCloseContainer').style.display="none";
    document.getElementById('chatWindowOpenContainer').style.display="block";
    document.getElementById('mainContent').style.paddingLeft="22%";
}

function chatOpen(){ 
    document.getElementById('chatWindowContainer').style.display="block";
    document.getElementById('chatWindowCloseContainer').style.display="block";
    document.getElementById('chatWindowOpenContainer').style.display="none";
    document.getElementById('mainContent').style.paddingLeft="30%";
}

function closeTOS(){ 
    document.getElementById('tosPopupContainer').style.display="none";
}

function openTOS(){
    document.getElementById('tosPopupContainer').style.display="block";
}

function openProvably(){
    document.getElementById('provablyFairContainer').style.display="block";
}

function closeProvably(){
    document.getElementById('provablyFairContainer').style.display="none";
}

function openAffiliates(){
    document.getElementById('affiliatePopupModalContainer').style.display="block";
}

function closeAffiliates(){
    document.getElementById('affiliatePopupModalContainer').style.display="none";
}



