<?php
    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');  
    require 'steamauth/steamauth.php';
    require 'steamauth/userInfo.php';
    require 'finduser.php';
    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');  
    session_start();

    $depositquery = "SELECT * FROM _transactions WHERE forUser = '".$_SESSION['steamid']."' AND transType = deposit";
    $withdrawquery = "SELECT * FROM _transactions WHERE forUser = '".$_SESSION['steamid']."' AND transType = withdraw";
    $marketplacequery = "SELECT * FROM _marketplace WHERE listedUser = '".$_SESSION['steamid']."'";
    $marketplacequery2 = "SELECT * FROM _marketplace WHERE purchasingUser = '".$_SESSION['steamid']."'";
    $statisticsquery = "SELECT * FROM _users WHERE steam64 = '".$_SESSION['steamid']."'";
    $bethistoryquery = "SELECT * FROM _bets WHERE placedUser = '".$_SESSION['steamid']."'";

    $depositsearch = mysqli_query($conn, $depositquery);
    $withdrawsearch = mysqli_query($conn, $withdrawquery);
    $marketplacesearch = mysqli_query($conn, $marketplacequery);
    $marketplacesearch2 = mysqli_query($conn, $marketplacequery2);
    $statisticssearch = mysqli_query($conn, $statisticsquery);
    $bethistorysearch = mysqli_query($conn, $bethistoryquery);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Methodbet - Profile</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="general.js"></script>
        <script src="profilejs.js"></script>
    </head>
        <?php include "header.php"; ?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <?php if(isset($_SESSION['steamid'])) { $userRank1 = $_SESSION['userRank'];?>
            <div class="profileDiv">
                <h1> Profile of <?=$_SESSION['userName']?></h1>  
                <div class="profileDivTop">
                    <img id = profilePic src="<?=$_SESSION['steam_avatarfull']?>">
                    <div class="profileInfoDiv">
                        <ul>
                            <li><h2>Level: <?=$_SESSION['userLevel']?></h2></li>
                            <li><h2>Rank: <?php if($userRank1 == 7) { echo "Owner";} elseif ($userRank1 == 0) { echo "User";} elseif ($userRank1 == 5) { echo "Admin";} else { echo "Moderator";} ?></h2></li>
                            <li><h2>Join Date: <?=$_SESSION['joinDate']?></h2></li>
                        </ul>
                    </div>
                    <div class= "userSettings">
                        <div class="userSettingsContent">
                            <h2>Update/Set Trade Link:</h2>
                            <a id="tradeLinkRedir" href="">Where do I get it?</a>
                            <form id="updateTradeLink" method="post" action="<?php $_SERVER['PHP_SELF']?>">
                                <input id="tradeLinkInputField" type="text" name="tradeLinkEnter" placeholder="Enter Trade Link Here.">
                                <input id="tradeLinkSubmitBTN" type="submit" value="Update Trade Link">
                                <?php $conn = mysqli_connect('localhost', 'methodical', '4Ip52+-7r8e3','methodic_website'); if (isset($_POST['tradeLinkEnter'])) { $tradeLink = mysqli_real_escape_string($conn, $_POST['tradeLinkEnter']); $tradeLinkInsert = mysqli_query($conn, "UPDATE _users SET tradelink=$tradeLink WHERE steam64 = '".$_SESSION['steamid']."'"); if ($tradeLinkInsert) { header('Location: profile.php'); echo "Updated Tradelink Successfully"; } else { echo "Error updating tradelink"; } } ?>
                            </form>
                        </div>
                    </div>
                 </div>                
            </div>
            <div class="profilePageSelectionDiv">
                <ul id="profileViewSelector">
                    <li id="depositsView"><a href="javascript:depositsOpen()">Non-marketplace Deposits</a></li>
                    <li id="withdrawsView"><a href="javascript:withdrawsOpen()">Non-marketplace Withdraws</a></li>
                    <li id="listingsView"><a href="javascript:listingsOpen()">Marketplace Listings</a></li>
                    <li id="purchasesView"><a href="javascript:purchasesOpen()">Marketplace Purchases</a></li>
                    <li id="statisticsView"><a href="javascript:statisticsOpen()">Statistics</a></li>
                    <li id="bethistoryView"><a href="javascript:openBetHistory()">Bet History</a></li>
                </ul>
            </div>
            <div id="profilePageContentDiv">
                <div id="profiledeposits">
                    <?php if ($depositsearch){ while($returnResults = mysqli_fetch_assoc($depositsearch)) { ?>
                        <li><h2>Transaction ID: <?=$returnResults['id'];?></h2><h3>Transaction Method: <?=$returnResults['transMethod'];?></h3><h2>Amount: <?=$returnResults['amount'];?></h2></li>
                    <?php } } else {?>
                        <h1>You do not have any recent deposits!</h1>
                    <?php } ?>
                </div>
                <div id="profilewithdraws">
                    <?php if ($withdrawsearch) { while($returnResults2 = mysqli_fetch_assoc($withdrawsearch)) { ?>
                        <li><h2>Transaction ID: <?=$returnResults2['id'];?></h2><h3>Transaction Method: <?=$returnResults2['transMethod'];?></h3><h2>Amount: <?=$returnResults2['amount'];?></h2></li>
                    <?php } } else {?>
                        <h1>You do not have any recent withdraws!</h1>
                    <?php } ?>
                </div>
                <div id="profilemarketplacelistings">
                    <?php while($returnResults3 = mysqli_fetch_assoc($marketplacesearch)) { ?>
                    <li><h2>Listing ID: <?=$returnResults3['listingID'];?></h2><h3>Item Name: <?=$returnResults3['itemName'];?> Condition: <?=$returnResults3['itemCondition'];?></h3><h2>Listed Price: <?=$returnResults3['price'];?> Status: <?php if($returnResults3['listingStatus'] == 2) { echo "Active"; ?><a href="javascript:openListingSettings()">Listing Settings</a><?php }elseif($returnResults3['listingStatus'] == 1) { echo "Completed"; } elseif($returnResults3['listingStatus'] == 0) { echo "Cancelled"; } ?></h2></li>
                    <?php } if (mysqli_num_rows($marketplacesearch) == 0) {?>
                        <h1>You do not have any marketplace listings!</h1>
                    <?php } ?>
                </div>
                <div id="profilemarketplacepurchases">
                    <?php while($returnResults4 = mysqli_fetch_assoc($marketplacesearch2)) { ?>
                        <li><h2>Listing ID: <?=$returnResults4['listingID'];?></h2><h3>Item Name: <?=$returnResults4['itemName'];?> Condition: <?=$returnResults4['itemCondition'];?></h3><h2>Purchasing Price: <?=$returnResults4['price'];?></h2></li>
                    <?php } if (mysqli_num_rows($marketplacesearch2) == 0) {?>
                        <h1>You do not have any marketplace listings!</h1>
                    <?php } ?>
                </div>
                <div id="profilestatistics">
                    <?php if ($statisticssearch) {while($returnResults5 = mysqli_fetch_assoc($statisticssearch)) { ?>
                        <li><h2>Bet Total: <?=$returnResults5['betTotal'];?> Used Refferal Code: <?=$returnResults5['$usedRefCode'];?>Redeemable Affiliate Balance: <?=$returnResults5['redeemableAffBal'];?></h2></li>
                    <?php } } else {?>
                        <h1>Error loading statistics</h1>
                    <?php } ?>
                </div>
                <div id="profilebethistory">
                    <table id="bethistorytable" style="width:90%">
                        <th>Bet ID</th><th>Bet Round</th><th>Color</th><th>Amount</th><th>Result</th>
                        <?php while($returnResults6 = mysqli_fetch_assoc($bethistorysearch)) { ?>
                            <tr><td><?=$returnResults6['id'];?></td><td><?=$returnResults6['betRound'];?></td><td><?=$returnResults6['betColor'];?></td><td><?=$returnResults6['amt'];?></td><td><?=$returnResults6['balanceResult'];?></td></tr>
                        <?php } if (mysqli_num_rows($bethistorysearch) == 0) {?>
                            <h1>You do not have any past bets!</h1>
                        <?php } ?>
                    </table>
                </div>
            </div>
        <?php } else { ?>
            <h1>You must be logged in to view your profile!</h1>
        <?php } ?>
    </body>
</html>