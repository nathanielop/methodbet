window.onload=function(){
    var chatWindowClose = document.getElementById('chatWindowClose');
    chatWindowClose.addEventListener("click", chatClose);
    var chatWindowOpen = document.getElementById('chatWindowOpen');
    chatWindowOpen.addEventListener("click", chatOpen);
    var tosCloseButton = document.getElementById('tosCloseButton');
    tosCloseButton.addEventListener("click", closeTOS);
    var tosOpenButton = document.getElementById('tosOpenButton');
    tosOpenButton.addEventListener("click", openTOS);
    var provablyOpenButton = document.getElementById('provablyOpenButton');
    provablyOpenButton.addEventListener("click", openProvably);
    var provablyCloseButton = document.getElementById('provablyCloseButton');
    provablyCloseButton.addEventListener("click", closeProvably);
    var affiliateOpenButton = document.getElementById('affiliatesOpenButton');
    affiliateOpenButton.addEventListener("click", openAffiliates);
    var affiliateCloseButton = document.getElementById('affiliatePopupModalCloseBTN');
    affiliateCloseButton.addEventListener("click", closeAffiliates);
    var csgoSelectButton = document.getElementById('csgoItemBTN');
    csgoSelectButton.addEventListener("click", selectCSGO);
    var bitcoinSelectButton = document.getElementById('bitcoinDepoBTN');
    bitcoinSelectButton.addEventListener("click", selectBitcoin);
    var g2aSelectButton = document.getElementById('g2aDepoBTN');
    g2aSelectButton.addEventListener("click", selectG2A);
}

function selectCSGO(){
    var csgodiv = document.getElementById('csgoDepositDiv');
    var bitcoindiv = document.getElementById('cryptoDepo');
    var g2adiv = document.getElementById('g2aDepo');
    csgodiv.style.display = "block";
    bitcoindiv.style.display = "none";
    g2adiv.style.display = "none";
}

function selectBitcoin(){
    var csgodiv = document.getElementById('csgoDepositDiv');
    var bitcoindiv = document.getElementById('cryptoDepo');
    var g2adiv = document.getElementById('g2aDepo');
    csgodiv.style.display = "none";
    bitcoindiv.style.display = "block";
    g2adiv.style.display = "none";
}

function selectG2A(){
    var csgodiv = document.getElementById('csgoDepositDiv');
    var bitcoindiv = document.getElementById('cryptoDepo');
    var g2adiv = document.getElementById('g2aDepo');
    csgodiv.style.display = "none";
    bitcoindiv.style.display = "none";
    g2adiv.style.display = "block";
}

function chatClose(){ 
    document.getElementById('chatWindowContainer').style.display="none";
    document.getElementById('chatWindowCloseContainer').style.display="none";
    document.getElementById('chatWindowOpenContainer').style.display="block";
    document.getElementById('depositDivContent').style.width="80%";
    document.getElementById('depositDivContent').style.left="0";
}

function chatOpen(){ 
    document.getElementById('chatWindowContainer').style.display="block";
    document.getElementById('chatWindowCloseContainer').style.display="block";
    document.getElementById('chatWindowOpenContainer').style.display="none";
    document.getElementById('depositDivContent').style.width="calc(60% - 4px)";
    document.getElementById('depositDivContent').style.left="calc(20% + 4px)";
}

function closeTOS(){ 
    document.getElementById('tosPopupContainer').style.display="none";
}

function openTOS(){
    document.getElementById('tosPopupContainer').style.display="block";
}

function openProvably(){
    document.getElementById('provablyFairContainer').style.display="block";
}

function closeProvably(){
    document.getElementById('provablyFairContainer').style.display="none";
}

function openAffiliates(){
    document.getElementById('affiliatePopupModalContainer').style.display="block";
}

function closeAffiliates(){
    document.getElementById('affiliatePopupModalContainer').style.display="none";
}
