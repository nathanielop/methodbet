<?php
    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');

    $query = "SELECT * FROM _marketplace";

    $search = mysqli_query($conn, $query);

    if (isset($_POST['itemname'])) {
        $searchFieldInput = mysqli_real_escape_string($_POST['itemName']);
        $query7 = "SELECT * FROM _marketplace WHERE itemName LIKE '%'$itemName'%'";
        $search7 = mysqli_query($conn, $query7);
    } 

    session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Methodbet - Marketplace</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    </head>
    <body>
        <?php include "header.php"?>
        <h1 class="marketplaceTitle">Marketplace</h1>
        <?php if(isset($_SESSION['steamid'])) { ?>
        <div class="WithdrawSearchBar">
            <h3 class="marketplaceDesc">Search for Items in the Marketplace</h3>
            <form class="searchItems" method="post" action="<?php $_SERVER['PHP_SELF']?>">
                <input type="text" name="itemName" placeholder="Dragon Lore">
                <input type="submit" value="Submit">
            </form>
            <form class="filterItems" method="post" action="<?php $_SERVER['PHP_SELF']?>">
                <select>
                    <option value="lowHigh">Low to High</option>
                    <option value="highLow">High to Low</option>
                </select>
                <input type="submit" value="Filter Results">
            </form>
        </div>
        <div class="marketplaceItems">
            <ul>
                <?php if (isset($_POST['itemname'])) { ?>
                <?php while($returnItemArray = mysqli_fetch_assoc($search7)) {?>
                    <li><h2><?=$returnItemArray["itemName"];?></h2><h3>Condition: <?php if($returnItemArray["itemCondition"] = 0){echo "Battle-Scarred";} elseif ($returnItemArray["itemCondition"] = 1){echo "Well Worn";} elseif ($returnItemArray["itemCondition"] = 2){echo "Field-Tested";} elseif ($returnItemArray["itemCondition"] = 3){echo "Minimal Wear";} else {echo "Factory New";}?></h3><img class="itemImage" src="<?=$returnItemArray["itemImageSrc"];?>"><h2>Price: <?=$returnItemArray["price"];?></h2><form method="post" action="<?php $_SERVER['PHP_SELF']?>"><input type="submit" value="Add to Cart"></form></li>
                <?php } if (mysqli_num_rows($returnItemArray) == 0) { echo "No Items Found"; var_dump($returnItemArray);} } else {?>
                    <?php while($returnItem = mysqli_fetch_assoc($search)) { ?>
                    <li><h2><?=$returnItem["itemName"];?></h2><h3>Condition: <?php if($returnItem["itemCondition"] = 0){echo "Battle-Scarred";} elseif ($returnItem["itemCondition"] = 1){echo "Well Worn";} elseif ($returnItem["itemCondition"] = 2){echo "Field-Tested";} elseif ($returnItem["itemCondition"] = 3){echo "Minimal Wear";} else {echo "Factory New";}?></h3><img class="itemImage" src="<?=$returnItem["itemImageSrc"];?>"><h2>Price: <?=$returnItem["price"];?></h2><form method="post" action="<?php $_SERVER['PHP_SELF']?>"><input type="submit" value="Add to Cart"></form></li>
                <?php } } ?>
            </ul>
        </div>
        <div class="checkOutSideBar">
            <h2>Cart</h2>
            <ul>
            </ul>
            <div class="shoppingCartBottomInfo">
                <?php if (isset($cartTotalPrice)) { ?>
                <h3>Total: <?=$cartTotalPrice?></h3>
                <form method="post" action="<?php $_SERVER['PHP_SELF']?>">
                    <input type="submit" value="Checkout">
                </form>
                <?php } else { ?>
                    <h3> You have not selected any items to withdraw yet! </h3>
                <?php } ?>
            </div>
        </div>
        <?php } else { ?>
            <p>You must be signed in to view the marketplace!</p>
        <?php } ?>
    </body>
</html>

    