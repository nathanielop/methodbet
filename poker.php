<?php
session_start();

if(isset($_SESSION['steamid'])){
    $id = $_SESSION['steamid'];
    $isLoggedIn = true;
    $_SESSION['userName'] = $steamprofile['personaname'];
    $getUserDataQuery = mysqli_query($conn, "SELECT * FROM _users WHERE steam64 = '".$id."'");
    $getUserDataReturn = mysqli_fetch_assoc($getUserDataQuery);
    $userBalance = $getUserDataReturn["balance"];
    $userLevel = $getUserDataReturn["lvl"];
    $userRank = $getUserDataReturn["userRank"];
    $userBetTotal = $getUserDataReturn["betTotal"];
    $joinDate = $getUserDataReturn["joinDate"];
    $isUserBanned = $getUserDataReturn["isUserBanned"];
    $_SESSION['userBalance'] = $userBalance;
    $_SESSION['userLevel'] = $userLevel;
    $_SESSION['userRank'] = $userRank;
    $_SESSION['joinDate'] = $joinDate;
    $_SESSION['isUserBanned'] = $isUserBanned;
    $userPicture = $_SESSION['steam_avatar'];
    $userPictureFull = $_SESSION['steam_avatarfull'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Hyper-bet - Poker</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="depositjs.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script>
            const socket = io('http://198.20.228.80:8002');
        </script>
    </head>     
    <body>
        <?php include "header.php"?>
        <?php include "chat.php"?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <div id="poker-contents">
            <div id="poker-heading">
                <h2 id="poker-heading-text">Hyper-bet Hold-em' Poker</h2>
            </div>
            <div id="poker-tournaments-container">
                <div id="poker-tournaments-heading-text">
                    <h3 id="poker-trny-heading-text-box">Tournaments</h3>
                </div>
                <div id="poker-tournaments-filter-options">
                </div>
                <div id="poker-tournaments-list-container">
                    <ul id="poker-tourneys-list">
                        <li class="poker-tourney-example-li"><h2 class="poker-tourney-li-title">New Year Mega Bonus</h2><ul class="poker-tourney-rules"><li class="poker-tourney-rules-li"><h5>$20 Buy-in</h5></li><li class="poker-tourney-rules-li"><h5>First place takes all</h5></li></ul><h3 class="poker-tourney-li-prize-first">$2,500</h3><button class="join-tourney-btn">Join Tournament</button></li>
                    </ul>
                </div>
            </div>
            <div id="poker-tables-container">
                <div id="poker-tables-heading-text">
                    <h3 id="poker-tables-heading-text-box">Poker Tables</h3>
                </div>
                <div id="poker-tables-filter-options">

                </div>
                <div id="poker-tables-list">
                    <li class="poker-table-list-item"><h2 class="table-limit">Low Roller Table</h2><h2 class="buy-in-limit">$1-$5</h2><button class="join-poker-table-btn">Join Table</button></li>
                    <li class="poker-table-list-item"><h2 class="table-limit">Medium Roller Table</h2><h2 class="buy-in-limit">$5-$20</h2><button class="join-poker-table-btn">Join Table</button></li>
                    <li class="poker-table-list-item"><h2 class="table-limit">High Roller Table</h2><h2 class="buy-in-limit">$20-$100</h2><button class="join-poker-table-btn">Join Table</button></li>
                    <li class="poker-table-list-item"><h2 class="table-limit">Super High Roller Table</h2><h2 class="buy-in-limit">$100-$500</h2><button class="join-poker-table-btn">Join Table</button></li>
                    <li class="poker-table-list-item"><h2 class="table-limit">Super-VIP Table</h2><h2 class="buy-in-limit">$500 - $2500</h2><button class="join-poker-table-btn">Join Table</button></li>
                    <li class="poker-table-list-item"><h2 class="table-limit">Royalty Table</h2><h2 class="buy-in-limit">Invite-Only</h2><?php if ($_SESSION['userLevel'] > 75) { ?><button class="join-poker-table-btn">Join Table</button><?php } ?></li>
                </div>
            </div>
        </div>
        <div id="buyin-options-panel">
            
        </div>
    </body>
</html>
    