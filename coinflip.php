<!DOCTYPE html>
<html>
    <head>
        <title>Hyper-bet - Poker</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script>
            const socket = io('http://198.20.228.80:8002');
        </script>
    </head>     
    <body>
        <?php include "header.php"?>
        <?php include "chat.php"?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <div id="coinflip-page-contents">
            <div id="coinflip-contents">
                <div id="coinflip-heading-div">
                    <h2 id="coinflip-heading">Coinflip</h2>
                </div>
                <div id="make-coinflip-game-div">
                    <div id="create-coinflip-heading-div">
                        <h3 id="create-coinflip-heading">Create a Game</h3>
                    </div>
                    <div id="active-coinflip-owned-games">
                        <ul id="active-coinflip-owned-games-list">
                            <li class="Headscoinflip"><h2 class="cf-amt">5.00</h2><button class="coinflip-options">Game Options</button></li>
                            <li class="Tailscoinflip"><h2 class="cf-amt">5.00</h2><button class="coinflip-options">Game Options</button></li>
                        </ul>
                    </div>
                </div>
                <div id="active-cf-games">
                    <ul id="active-coinflip-unowned-games">
                        <li class="Headscoinflip"><h2 class="cf-amt">5.00</h2><button class="coinflip-join-btn">Join Game</button></li>
                    </ul>
                </div>
                <div id="past-cf-games">
                    <button id="show-past-cf-games">History</button>
                    <ul id="past-cf-games-list">
                        <li class="past-coinflip-li-example"><h2 class="cf-amt">5.00</h2><img class="cf-winning-side-div"></img></li>
                    </ul>
                </div>
            </div>
            <div id="coinflip-owned-game-options">
                <ul id="coinflip-game-options-panel">
                    <li><button id="cf-delete-game-btn">Delete Game</button></li>
                    <li><button id="cf-duplicate-game-btn">Duplicate Game</button></li>
                    <li><button id="cf-private-game">Private Game</button></li>
                    <li><button id="cf-other-game-options">Other Options</button></li>
                </ul>
            </div>
        </div>
    </body>
</html>
    