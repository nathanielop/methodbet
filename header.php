
<body>
        <header>
            <div id="titleNav">
                    <div id="mainBrand"><nav><a href="index">Hyperbet</a></div>
                    <div id="gameSelect">
                        <ul>
                            <li id="stdRoulette"><a href="index"><h3>Roulette</h3></a>
                            <li id="megaRoulette"><a href="megaroulette"><h3>Mega Roulette</h3></a>
                            <?php if($_SESSION['userRank']>0){ ?>
                            <li id="jackpot"><a href="jackpot"><h3>Jackpot</h3></a>
                            <li id="poker"><a href="poker"><h3>Poker</h3></a>
                            <li id="coinFlip"><a href="coinflip"><h3>Coinflip</h3></a>
                            <?php } ?>
                        </ul>
                    </div>
                    <div id="navBarHugRight">
                        <ul id="secondaryNavBar">
                            <li id="provablyFair"><a id="provablyOpenButton" href="javascript:openProvably()">Provably Fair</a></li>
                            <li id="affiliates"><a id="affiliatesOpenButton" href="javascript:openAffiliates()">Affiliates</a></li>
                            <li id="termsofservice"><a id="tosOpenButton" href="javascript:openTOS()">Terms of Service</a></li>
                            <?php if (isset($_SESSION['steamid'])) { ?>
                            <li id="logout"><a href="?logout">Logout</a></li>
                            <?php } ?>
                        </ul>
                        <ul id="mainNavBar">
                            <li id="signIn">                
                                <? if(isset($_SESSION['steamid'])) { ?>
                                    <a href="profile"><?=$_SESSION['userName'];?></a>
                                <? } else {?>
                                    <a href="index.php?login">Sign In</a> 
                                <? } ?></li>
                            <li id="withdraw"><a href="marketplace">Marketplace</a></li>

                            <li id="deposit"><a href="deposit">Deposit</a></li>
                            <?php if (isset($_SESSION['steamid'])) { if ($_SESSION['userRank'] > 0) {?>
                                <style> #controlPanel { width: 50px; }</style>
                                <li id="controlPanel"><a href="controlPanel">cPanel</a></li>
                            <?php } else {}} ?> 
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
