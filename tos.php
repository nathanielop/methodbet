<div id="tosPopupContainer">
    <div id="tosPopupModal">
        <div id="tosPopupModalContent">
            <h2>Terms of Service</h3>
            <p><strong>By using Hyper-bet, you acknowledge our following terms of service</strong></p>
            <ul id="tosListOfRules">
                <li class="tosRulesListItem"><h5><strong>I. (General Statement)</strong> By using our website and any of its associated features, you acknowledge that you are 18 years or older, and that any presentation of evidence which may point to this being inaccurate can lead to sudden account closures without return of funds.</h5></li>
                <li class="tosRulesListItem"><h5><strong>II. (Technical Issues)</strong> Any technical issues which may arise which result in the loss of balance or are otherwise impeding use of the site can be reported to our support team and should result in the returning of any lost funds.</h5></li>
                <li class="tosRulesListItem"><h5><strong>III. (Reasons for Closures)</strong> Accounts on Hyper-bet.com and any of its associated companies can be closed at any time without returning of any user funds or items, these can happen for several reasons such as aforementioned falsification of age verification documents, attempts to infiltrate the sites web servers or manipulate our provably fair system, or otherwise interferring with websites operations. Accounts closed will always be given a reason as to the closure, and can be appealed through our support system.</h5></li>
            </ul>
            <a id="tosCloseButton" href="javascript:closeTOS()">Close</a>
        </div>
    </div>
</div>
