<?php
/*
    if (isset($_POST['message'])) {
        $chatroom = 'General Chatroom';
        $messageContent = mysqli_real_escape_string($_POST['message']);
        $addChatMessageParam = "INSERT INTO _chatMessages (content, postedUser, postedChatroom) VALUES ($messageContent, $id, $chatroom)";
        if (mysqli_query($conn, $addChatMessageParam)) {
            header('Location: index.php');
        } else {
            echo "Error posting message";
        }
    }

*/
?>
<script>
    const out = document.getElementsByClassName("chatMessages")
    let c = 0

setInterval(function() {
    // allow 1px inaccuracy by adding 1
    const isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1

    // scroll to bottom if isScrolledToBottom is true
    if (isScrolledToBottom) {
      out.scrollTop = out.scrollHeight - out.clientHeight
    }
}, 500)

function format () {
  return Array.prototype.slice.call(arguments).join(' ')
}
</script>
<div id="chatWindowContainer">
    <div id="chatWindow">
        <h1 class="chatTitle">Chat</h1>
        <div class="chatMessages">
            <ul class="chatMessageList">
            </ul>
        </div>
        <?php if ($_SESSION['userRank'] > 0) { ?>
        <div id="chatMessageAdminOptionsPanel">
            <ul id="chatAdminOptions">
                <li href=""><h3>Timeout</h3></li>
                    <?php if ($_SESSION['userRank'] == 5 or $_SESSION['userRank'] == 7) { ?>
                        <li href=""><h3>Ban</h3></li>
                    <?php } ?>
            </ul>
        </div>
        <div id="timeoutFormDiv">
            <form id="timeoutForm" method="post" action="<?php $_SERVER['PHP_SELF'];?>">
                <h2>Reason for Ban:</h2>
                <input type="text" name="timeoutReason">
                <h2>Length of Ban:</h2>
                <input type="number" name="timeoutLength">
                <input type="submit" value="Timeout User">
                <h2>You're about to timeout this user, are you sure?</h2>
            </form>
            </div>
            <?php if ($_SESSION['userRank'] == 5 or $_SESSION['userRank'] == 7) {?>
            <div id="banFormDiv">
                <form id="banForm" method="post" action="<?php $_SERVER['PHP_SELF'];?>">
                    <h2>Reason for Ban:</h2>
                    <input type="text" name="banReason">
                    <input type="submit" value="Ban User">
                    <h2>You're about to ban this user, are you sure?</h2>
                </form>
            </div>
            <?php } ?>
            <?php } ?>
            <div id="chatMessageOptionsPanel">
                <ul id="chatUserOptions">
                    <li href=""><h3>Send Coins</h3></li>
                    <li href=""><h3>Mute</h3></li>
                </ul>    
            </div>
        <div class="chatSubmit">
            <?php if($isLoggedIn == true && $isUserBanned != true) { ?>
                <form class="chatMessageSubmitForm" action="">
                    <input id="messageField" type="text" name="message" placeholder="Type message...">
                    <input id="submitMessageField" type="submit" value="Submit">
                </form>
            <?php } elseif ($isLoggedIn == true && $isUserBanned == true) { ?>
                <h2 class="userChatNotification">You're currently banned or muted and are unable to chat, if you believe this to be an error, contact support.</h2>
            <?php } elseif ($isLoggedIn != true) { ?>
                <h2 class="userChatNotification">You must be logged in to chat!</h2>
            <?php } ?>
        </div>
    </div>
</div>
<div id="chatWindowOpenContainer">
    <div id="chatWindowOpen">
        <a href="javascript:chatOpen()">></a>
    </div>
</div>
<div id="chatWindowCloseContainer">
    <div id="chatWindowClose">
        <a href="javascript:chatClose()"><</a>
    </div>
</div>
<script>    
    var userName = "<?=$_SESSION['userName'];?>"
    var userTag = "<?php if($userRank == 7) { echo "[Owner]";} elseif ($userRank == 3) { echo "[Mod]";} elseif ($userRank == 5) { echo "[Admin]";} else {}?>"
    var userPic = "<?=$userPicture?>"

            $('.chatMessageSubmitForm').submit(function(e){
                e.preventDefault();
                e.stopPropagation();
                socket.emit('chatMessage', { message: $('#messageField').val(), username: userName, tag: userTag, userpiclink: userPic});
                $('#messageField').val('');
                return false;
            });
            socket.on('chatMessage', function(msg){
                $('.chatMessageList').append($('<li><div><img src="'+ msg.postpiclink + '"class="chatMessageUserPicture"><h2 class="chatUserName">' + msg.sendertag + msg.sendname + '</h2><?php if($userRank > 0) { ?><a class="chatMessageAdminOptions" href="openModPanel()">Mod</a> <?php } ?><a class="chatMessageOptions" href="openOptionsPanel()">Options</a><p class="chatMessageContentText">'+ msg.sendmessage + '</p></div>'));
            });
</script>