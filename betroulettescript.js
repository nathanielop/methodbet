    var blackBetButton = document.getElementById('blackBetButton')
    var greenBetButton = document.getElementById('greenBetButton')
    var redBetButton = document.getElementById('redBetButton')
    var betAmountVar = $('#userBetAmountInNum').val()
    var balanceField = document.getElementById('gameBalance')

    var halftimesbetBTN = document.getElementById('halftimesbetBTN')
    var twotimesbetBTN = document.getElementById('twotimesbetBTN')
    var maxbetBTN = document.getElementById('maxbetBTN')
    var clearbetBTN = document.getElementById('clearbetBTN')

    blackBetButton.addEventListener("click", blackBet);
    redBetButton.addEventListener("click", redBet);
    greenBetButton.addEventListener("click", greenBet);
    halftimesbetBTN.addEventListener("click", halfBet);
    twotimesbetBTN.addEventListener("click", doubleBet);
    maxbetBTN.addEventListener("click", maxBet);
    clearbetBTN.addEventListener("click", clearBet);

    blackBetButton.disabled = true;
    blackBetButton.style.cursor = "not-allowed";
    greenBetButton.disabled = true;
    greenBetButton.style.cursor = "not-allowed";
    redBetButton.disabled = true;
    redBetButton.style.cursor = "not-allowed";

    function halfBet(){
        currentBetAmt = $('#userBetAmountInNum').val();
        newBetAmt = currentBetAmt * .5;
        $('#userBetAmountInNum').val(newBetAmt);
    }

    function doubleBet(){
        currentBetAmt = $('#userBetAmountInNum').val();
        newBetAmt = currentBetAmt * 2;
        $('#userBetAmountInNum').val(newBetAmt);
    }

    function maxBet(){
        $('#userBetAmountInNum').val(userBalance);
    }

    function clearBet(){
        $('#userBetAmountInNum').val(0);
    }

    function blackBet(){
        if ($('#userBetAmountInNum').val() == 0) {
            window.alert("You must enter a value to bet!");
        } else if ($('#userBetAmountInNum').val() > 2500.00){
            window.alert("The maximum bet is 2,500.00.");
        } else if ($('#userBetAmountInNum').val() > userBalance){
            window.alert("You do not have enough balance to place this bet!");
        } else {
            let preroundedBetAmount = $('#userBetAmountInNum').val();
            let roundedBetAmount =Math.round(preroundedBetAmount * 100) / 100;
            window.userBalance = userBalance - roundedBetAmount;
            socket.emit('userBetBlack', { betAmount: roundedBetAmount, userbalance: userBalance, userid: user64id, username: userName, tag: userTag, userpiclink: userPic});
            $('#userBetAmountInNum').val(0);
            balanceField.childNodes[0].textContent = "Balance: " + userBalance; 
            return false;
        }
    }
    function redBet(){
        if ($('#userBetAmountInNum').val() == 0) {
            window.alert("You must enter a value to bet!");
        } else if ($('#userBetAmountInNum').val() > 2500.00){
            window.alert("The maximum bet is 2,500.00.");
        } else if ($('#userBetAmountInNum').val() > userBalance){
            window.alert("You do not have enough balance to place this bet!");
        } else {
            let preroundedBetAmount = $('#userBetAmountInNum').val();
            let roundedBetAmount =Math.round(preroundedBetAmount * 100) / 100;
            window.userBalance = userBalance - roundedBetAmount;
            socket.emit('userBetRed', { betAmount: roundedBetAmount, userbalance: userBalance, userid: user64id, username: userName, tag: userTag, userpiclink: userPic});
            $('#userBetAmountInNum').val(0);
            balanceField.childNodes[0].textContent = "Balance: " + userBalance; 
            return false;
        }
    }
    function greenBet(){
        if ($('#userBetAmountInNum').val() == 0) {
            window.alert("You must enter a value to bet!");
        } else if ($('#userBetAmountInNum').val() > 2500.00){
            window.alert("The maximum bet is 2,500.00.");
        } else if ($('#userBetAmountInNum').val() > userBalance){
            window.alert("You do not have enough balance to place this bet!");
        } else {
            let preroundedBetAmount = $('#userBetAmountInNum').val();
            let roundedBetAmount =Math.round(preroundedBetAmount * 100) / 100;
            window.userBalance = userBalance - roundedBetAmount;
            socket.emit('userBetGreen', { betAmount: roundedBetAmount, userbalance: userBalance, userid: user64id, username: userName, tag: userTag, userpiclink: userPic});
            $('#userBetAmountInNum').val(0);
            balanceField.childNodes[0].textContent = "Balance: " + userBalance; 
            return false;
        }
    }
    /*
    socket.on('userBetSuccess', function(userBetHasSucceeded){
        var userBalanceField = document.getElementById('gameBalance');
        userBalanceField.childNodes[0].textContent = "Balance: " + newBalance6; 
    });*/

    socket.on('newRoundData', function(transferNewRoundData){
        var RouletteListSpinner = document.getElementById("roulette-images-list");
        var roundParamField = document.getElementById("rouletteHashInformation");
        var blackBetList = document.getElementById("betListBlack");
        var redBetList = document.getElementById("betListRed");
        var greenBetList = document.getElementById("betListGreen");
        roundParamField.childNodes[0].textContent = "Round Seed: " + transferNewRoundData.roundseed + " (Secret: " + transferNewRoundData.roundsecret + ")";
        blackBetList.innerHTML = "";
        redBetList.innerHTML = "";
        greenBetList.innerHTML = "";
        setTimeout(function(){
            RouletteListSpinner.style.left= "0px";
        }, 4000);
        blackBetButton.disabled = false;
        blackBetButton.style.cursor = "pointer";
        greenBetButton.disabled = false;
        greenBetButton.style.cursor = "pointer";
        redBetButton.disabled = false;
        redBetButton.style.cursor = "pointer";
        var timeleft = 25.00;
        var rollTimer = setInterval(function(){
            let timeUntilUnfixed = Math.round(timeleft * 100) / 100;
            let timeUntil = timeUntilUnfixed.toFixed(2);
            document.getElementById("countdownTimer").childNodes[0].textContent = "Next Round in: " + timeUntil;
            timeleft -= .01;
            if(timeleft <= 0){
                clearInterval(rollTimer);
                document.getElementById("countdownTimer").childNodes[0].textContent = "Rolling!"
            }
        }, 10);
    });

    socket.on('userNewBalancePostRoundReturn', function(importNewBalance){
        let newBalance12 = importNewBalance.newUserBal;
        setTimeout(function() {
            window.userBalance = newBalance12;
            var userBalanceField = document.getElementById("gameBalance");
            userBalanceField.childNodes[0].textContent = "Balance: " + newBalance12;
        }, 4000)
    });

    socket.on('previousRoundData', function(addPrevData){
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.firstRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.secondRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.thirdRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.fourthRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.fifthRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.sixthRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.seventhRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.eigthRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.ninthRoundResult +'"></li>'));
        $('#previousRoundsUL').append($('<li class="previousRound'+ addPrevData.tenthRoundResult +'"></li>'));
    });

    socket.on('roundFinished', function(spinRouletteWheelToTicket){
        socket.emit('clientRequestingNewBalanceInformation', {user64id: user64id});
        var rouletteSpinnerCover = document.getElementById("roulette-container-cover-div");

        blackBetButton.disabled = true;
        blackBetButton.style.cursor = "not-allowed";
        greenBetButton.disabled = true;
        greenBetButton.style.cursor = "not-allowed";
        redBetButton.disabled = true;
        redBetButton.style.cursor = "not-allowed";

        var RouletteListSpinner = document.getElementById("roulette-images-list");

        rouletteSpinnerCover.style.opacity="0";

        setTimeout(function(){
        let newColor = spinRouletteWheelToTicket.roundColor;
        var previousUL = document.getElementById("previousRoundsUL")
        $('#previousRoundsUL').append($('<li class="previousRound'+ newColor +'"></li>'));
        var elements = previousUL.getElementsByTagName('li');
        previousUL.removeChild(elements[0]);
        rouletteSpinnerCover.style.animation= "fadein2 3s";
        }, 4000);

        var movementamount =  (spinRouletteWheelToTicket.roundTicket * 115) + 5275.405;

        RouletteListSpinner.style.left = "-" + movementamount + "px"
        
    });
