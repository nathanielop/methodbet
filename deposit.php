<?php
    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');
    session_start();

    if (empty($_SESSION['userInventoryLoadStatus'])) {
        $urlInventory = file_get_contents("http://steamcommunity.com/profiles/".$_SESSION['steamid']."/inventory/json/730/2");
        $inventoryContent = json_decode($urlInventory, true);
    }
    
    if (empty($_SESSION['userInventoryLoadStatus'])) {
        $priceDataSheet = file_get_contents("https://trial.steamanalyst.com/v2/WwLTDotdRtkbGDxFc");
        $priceContent = json_decode($priceDataSheet, true);
        $_SESSION['userInventoryLoadStatus'] = 1;
    }

    session_start();

    if (isset($_SESSION['steamid'])) {
        $isLoggedIn == true;
        $isUserBanned == false;
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Hyperbet - Deposit</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="depositjs.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script>
            const socket = io('http://198.20.228.80:8002');
        </script>
    </head>
    <body>
        <?php include "header.php"?>
        <?php include "chat.php"?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <div id="depositDivContent">
            <h1 class="marketplaceTitle2">Deposit</h1>
            <h2>Deposit Options</h2>
                <ul id="depositOptions">
                    <li><button id="csgoItemBTN">CSGO Items</button></li>
                    <li><button id="bitcoinDepoBTN">Cryptocurrency</button></li>
                    <li><button id="g2aDepoBTN">G2A Pay</button></li>
                </ul>
            <div id="csgoDepositDiv">
                <div class="DepositSearchBar">
                    <h3>Search for Items to Deposit</h3>
                    <form method="post" action="<?php $_SERVER['PHP_SELF'];?>">
                        <input type="text" name="itemName" placeholder="Dragon Lore">
                        <input type="submit" value="Submit">
                    </form>
                    <form method="post" action="<?php $_SERVER['PHP_SELF'];?>">
                        <select>
                            <option value="lowHigh">High to Low</option>
                            <option value="highLow">Low to High</option>
                        </select>
                        <input type="submit" value="Filter Results">
                    </form>
                </div>
                <div id="inventory">
                    <ul id="inventoryItems">
                        <?php if (!isset($inventoryContent["Error"])) { foreach ($inventoryContent['rgDescriptions'] as $item) { if ($item["marketable"] == 1) { $itemNameSearch = $item["market_hash_name"];?>
                        <li class="<?php if ($item["tradable"] == 0) {echo "tradebanned";} else {echo "normalli";}?>"><img src="https://steamcommunity-a.akamaihd.net/economy/image/<?=$item["icon_url"];?>"><h2><?=$item["market_hash_name"];?></h2><h2 class="priceField">Price: <?php echo $priceContent["'$item["market_hash_name"]'"]["avg_price_30_days"];?></h2><button>List on Marketplace</button></li>
                        <?php } else { } } } else { echo "There was an error loading your inventory"; }?>
                    </ul> 
                </div>  
            </div>          
            <div id="cryptoDepo">
                <h2>Deposit Purchasing Guide: $1 = 1000 coins, if you'd like to purchase $50 worth of coins, enter 50000</h2>
                <form method="post" action="">
                    <input type="number" name="depositAmount" placeholder="500">
                    <input type="submit" value="Deposit">
                </form>
                <h2>This method of deposit has not been set up yet!</h2>
            </div>    
            <div id="g2aDepo">
                <h2>Deposit Purchasing Guide: $1 = 1000 coins, if you'd like to purchase $50 worth of coins, enter 50000</h2>
                <form method="post" action="<?php $_SERVER['PHP_SELF'];?>">
                    <input type="number" name="g2adepositAmount" placeholder="500">
                    <input type="submit" value="Deposit">
                </form>
                <h2>This method of deposit has not been set up yet!</h2>
            </div>  
        </div>
        <div id="listOptionsModalDiv">
            <div id="background-cover-wall-div"></div>
            <h2 id="listItemName">Listing options for LISTITEMNAME.</h2>
            <h3 id="featuredStatus">Would you like to feature this item for 5 coins? (This can drastically decrease the time it takes to sell)</h3><input type="checkbox" id="featuredCheckbox">
            <h3 id="listingPremium">Would you like to add increase the price of this item above market value? (This can make an item take longer to sell)</h3>
            <input type="range" min="0.00" max="0.15" step=".01">
            <button id="confirmItemListingBTN">Confirm Listing</button>
        </div>
        <div id="successfulListingDiv">
            <div id="background-cover-wall-div"></div>
            <h2 id="successfulHeading">Congratulations! Your item has been listed on the marketplace.</h2>
            <h4 id="successfulText">You can view this items status through the marketplace page. If you decide at any time to cancel the listing or change any properties of the listing, you can also access those on the marketplace page by navigating to My Listings > Listing Options. (Note: If you chose to feature your items and then chose to cancel your listing, your credits will be returned.)</h2>
            <button id="closeSuccessPopup">Close Popup</button>
        </div>
    </body>
</html>