   <div id="affiliatePopupModalContainer">
        <div id="affiliatePopupModal">
            <div id="affiliatePopupModalContent">
                <h1>Affiliate Settings</h1>
                <h2>Redeem Affiliate Code</h1>
                <form id="useAffiliateCode" action="">
                    <input id="useAffCodeTXTfield" type="text" name="redeemAffiliateCode">
                    <input id="useAffCodeSubmitBTN" type="submit" value="Submit">
                </form>
                <h2>Set Affiliate Code</h1>
                <form id="setAffiliateCode" action="">
                    <input id="setAffCodeTXTfield" type="text" name="usersAffiliateCode">
                    <input id="setAffCodeSubmitBTN" type="submit" value="Submit">
                </form>
                <a id="affiliatePopupModalCloseBTN" href="javascript:closeAffiliates()">Close</a>
            </div>
        </div>
    </div>
<script>

    $('#setAffiliateCode').submit(function(e){
        let userFieldContents = $('#setAffCodeTXTfield').val();
            e.preventDefault();
            e.stopPropagation();
        if (!userFieldContents) {
            window.alert("You must enter a code to set!");
        } else {
            socket.emit('userSetAffiliateCode', {userid: user64id, newaffcode: userFieldContents});
            $('#setAffCodeTXTfield').val('');
            return false;
        }
    });

    $('#useAffiliateCode').submit(function(e){
        let userFieldContents = $('#useAffCodeTXTfield').val();
            e.preventDefault();
            e.stopPropagation();
        if (!userFieldContents) {
            window.alert("You must enter a code to use!");
        } else {
            socket.emit('userUseAffiliateCode', {userid: user64id, usedaffcode: userFieldContents});
            $('#useAffCodeTXTfield').val('');
            return false;
        }
    });

    socket.on('successfulAffCodeUpdate', function(hasSetAffCode){
        window.alert("Successfully set affiliate code");
    });

    socket.on('notUniqueCode', function(notUniqueAffCode){
        window.alert("This affiliate code has already been taken");
    });

    socket.on('noUserWithRefCodeSpecified', function(nonExistentAffCode){
        window.alert("There is no user with this affiliate code");
    });

    socket.on('userHasAlreadyRedeemedCode', function(duplicateAffCodeUse){
        window.alert("You have already redeemed an affiliate code");
    });

    socket.on('userHasRedeemedAffCodeSuccessfully', function(usedAffCode){
        window.alert("Successfully used affiliate code, .5 coins have been added to your balance!");
    });
</script>