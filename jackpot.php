<?php
    require 'steamauth/steamauth.php';
    require 'steamauth/userInfo.php';

    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');    

    session_start();

    if(isset($_SESSION['steamid'])){
        $id = $_SESSION['steamid'];
        $isLoggedIn = true;
        $_SESSION['userName'] = $steamprofile['personaname'];
        $getUserDataQuery = mysqli_query($conn, "SELECT * FROM _users WHERE steam64 = '".$id."'");
        $getUserDataReturn = mysqli_fetch_assoc($getUserDataQuery);
        $userBalance = $getUserDataReturn["balance"];
        $userLevel = $getUserDataReturn["lvl"];
        $userRank = $getUserDataReturn["userRank"];
        $userBetTotal = $getUserDataReturn["betTotal"];
        $joinDate = $getUserDataReturn["joinDate"];
        $isUserBanned = $getUserDataReturn["isUserBanned"];
        $_SESSION['userBalance'] = $userBalance;
        $_SESSION['userLevel'] = $userLevel;
        $_SESSION['userRank'] = $userRank;
        $_SESSION['joinDate'] = $joinDate;
        $_SESSION['isUserBanned'] = $isUserBanned;
        $userPicture = $_SESSION['steam_avatar'];
        $userPictureFull = $_SESSION['steam_avatarfull'];
    }

    require 'finduser.php';

    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');   

    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Hyperbet</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="general.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
        <script>
            var userName = "<?=$_SESSION['userName'];?>"
            var userTag = "<?php if($userRank == 7) { echo "[Owner]";} elseif ($userRank == 3) { echo "[Mod]";} elseif ($userRank == 5) { echo "[Admin]";} else {}?>"
            var userPic = "<?=$userPicture?>"
            var userSteamID = "<?=$_SESSION['steamid']?>"
            var userBalance = "<?=$userBalance?>"
            var userbalance = "<?=$userBalance?>"
            var user64id = "<?=$_SESSION['steamid']?>"
            const socket = io('http://198.20.228.80:8002');
            socket.on('error', function(e){
                console.log('error', e);
            });
        </script>
    </head>
        <?php include "header.php"?>
        <?php include "chat.php"?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <div id="mainJackpotContent">
            <div id="jackpotGameSelect">
                <h3 id="jackpotGameSelectHeading">Jackpot Rooms</h3>
                <ul id="jackpotGameSelectUL">
                    <li id="VSBTN"><button>Very Small<br>($1 - $5 deposits)</button></li>
                    <li id="SBTN"><button>Small<br>($5 - $10 deposits)</button></li>
                    <li id="MBTN"><button>Medium<br>($10 - $50 deposits)</button></li>
                    <li id="LBTN"><button>Large<br>($50 - $250 deposits)</button></li>
                    <li id="RBTN"><button>Royalty<br>($100 - Any deposits)</button></li>
                </ul>
            </div>
            <div id="jackpotVSRoomContents" class="jackpotRoomContents">
                <div class="jackpotRoundDiv">
                    <h3 class="jackpotRoomCountdown">Next round in: </h3>
                    <h3 class="jackpotRoomStatus">There must be at least two players to start countdown...</h3>
                    <div class="jackpotSpinner">
                    </div>
                    <div class="jackpotRoundInformation">
                        <h5 id="jackpotRoomVSRoundInformation">Round Seed: (Round Secret: )</h5>
                    </div>
                    <div class="jackpotDepositDiv">
                        <h2 class="jackpotDepositHeading">Join Jackpot</h2>
                        <div class="depositOptionsPopup">
                            <h3 class="depositOptionsHeading">Deposit Options</h3>
                            <input type="number" placeholder="Please enter a number between 1 and 5" min="1" max="5">
                            <button id="depositConfirmVSBTN">Join Jackpot!</button>
                        </div>
                    </div>
                    <div id="jackpotDepositsVSList">
                        <ul id="jackpotDepositsVSListUL"><li class="jackpotDepositsLI"></li></ul>
                    </div>
                </div>
            </div>
            <div id="jackpotSRoomContents" class="jackpotRoomContents">
                <div class="jackpotRoundDiv">
                    <h3 class="jackpotRoomCountdown">Next round in: </h3>
                    <h3 class="jackpotRoomStatus">There must be at least two players to start countdown...</h3>
                    <div class="jackpotSpinner">
                    </div>
                    <div class="jackpotRoundInformation">
                        <h5 id="jackpotRoomSRoundInformation">Round Seed: (Round Secret: )</h5>
                    </div>
                    <div class="jackpotDepositDiv">
                        <h2 class="jackpotDepositHeading">Join Jackpot</h2>
                        <div class="depositOptionsPopup">
                            <h3 class="depositOptionsHeading">Deposit Options</h3>
                            <input type="number" placeholder="Please enter a number between 5 and 10" min="5" max="10">
                            <button id="depositConfirmSBTN">Join Jackpot!</button>
                        </div>
                    </div>
                    <div id="jackpotDepositsSList">
                        <ul id="jackpotDepositsSListUL"><li class="jackpotDepositsLI"></li></ul>
                    </div>
                </div>
            </div>
            <div id="jackpotMRoomContents" class="jackpotRoomContents">
                <div class="jackpotRoundDiv">
                    <h3 class="jackpotRoomCountdown">Next round in: </h3>
                    <h3 class="jackpotRoomStatus">There must be at least two players to start countdown...</h3>
                    <div class="jackpotSpinner">
                    </div>
                    <div class="jackpotRoundInformation">
                        <h5 id="jackpotRoomMRoundInformation">Round Seed: (Round Secret: )</h5>
                    </div>
                    <div class="jackpotDepositDiv">
                        <h2 class="jackpotDepositHeading">Join Jackpot</h2>
                        <div class="depositOptionsPopup">
                            <h3 class="depositOptionsHeading">Deposit Options</h3>
                            <input type="number" placeholder="Please enter a number between 10 and 50" min="10" max="50">
                            <button id="depositConfirmMBTN">Join Jackpot!</button>
                        </div>
                    </div>
                    <div id="jackpotDepositsMList">
                        <ul id="jackpotDepositsMListUL"><li class="jackpotDepositsLI"></li></ul>
                    </div>
                </div>
            </div>
            <div id="jackpotLRoomContents" class="jackpotRoomContents">
                <div class="jackpotRoundDiv">
                    <h3 class="jackpotRoomCountdown">Next round in: </h3>
                    <h3 class="jackpotRoomStatus">There must be at least two players to start countdown...</h3>
                    <div class="jackpotSpinner">
                    </div>
                    <div class="jackpotRoundInformation">
                        <h5 id="jackpotRoomLRoundInformation">Round Seed: (Round Secret: )</h5>
                    </div>
                    <div class="jackpotDepositDiv">
                        <h2 class="jackpotDepositHeading">Join Jackpot</h2>
                        <div class="depositOptionsPopup">
                            <h3 class="depositOptionsHeading">Deposit Options</h3>
                            <input type="number" placeholder="Please enter a number between 50 and 250" min="50" max="250">
                            <button id="depositConfirmLBTN">Join Jackpot!</button>
                        </div>
                    </div>
                    <div id="jackpotDepositsLList">
                        <ul id="jackpotDepositsLListUL"><li class="jackpotDepositsLI"></li></ul>
                    </div>
                </div>
            </div>
            <div id="jackpotRRoomContents" class="jackpotRoomContents">
                <div class="jackpotRoundDiv">
                    <h3 class="jackpotRoomCountdown">Next round in: </h3>
                    <h3 class="jackpotRoomStatus">There must be at least two players to start countdown...</h3>
                    <div class="jackpotSpinner">
                    </div>
                    <div class="jackpotRoundInformation">
                        <h5 id="jackpotRoomRRoundInformation">Round Seed: (Round Secret: )</h5>
                    </div>
                    <?php if ($_SESSION['userLevel'] > 50) { ?>
                    <div class="jackpotDepositDiv">
                        <h2 class="jackpotDepositHeading">Join Jackpot</h2>
                        <div class="depositOptionsPopup">
                            <h3 class="depositOptionsHeading">Deposit Options</h3>
                            <input type="number" placeholder="Please enter a number over 100" min="100">
                            <button id="depositConfirmRBTN">Join Jackpot!</button>
                        </div>
                    </div>
                    <?php } else { ?>
                        <h2 class="jackpotDepositHeading">You must be over level 50 to join this jackpot!</h2>
                    <?php } ?>
                    <div id="jackpotDepositsRList">
                        <ul id="jackpotDepositsRListUL"><li class="jackpotDepositsLI"></li></ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="general.js"></script>
    <script type="text/javascript" src="jackpotpagescript.js"></script>
</html>