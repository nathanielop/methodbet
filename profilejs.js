window.onload=function(){
    var deposits = document.getElementById('depositsView');
    deposits.addEventListener("click", depositsOpen);
    var withdraws = document.getElementById('withdrawsView');
    withdraws.addEventListener("click", withdrawsOpen);
    var listings = document.getElementById('listingsView');
    listings.addEventListener("click", listingsOpen);
    var purchases = document.getElementById('purchasesView');
    purchases.addEventListener("click", purchasesOpen);
    var statistics = document.getElementById('statisticsView');
    statistics.addEventListener("click", statisticsOpen);
    var bethistory = document.getElementById('bethistoryView');
    bethistory.addEventListener("click", openBetHistory);
}

function depositsOpen() {
    document.getElementById('profiledeposits').style.display="block";
    document.getElementById('profilewithdraws').style.display="none";
    document.getElementById('profilemarketplacelistings').style.display="none";
    document.getElementById('profilemarketplacepurchases').style.display="none";
    document.getElementById('profilestatistics').style.display="none";
    document.getElementById('profilebethistory').style.display="none";
}

function withdrawsOpen() {
    document.getElementById('profiledeposits').style.display="none";
    document.getElementById('profilewithdraws').style.display="block";
    document.getElementById('profilemarketplacelistings').style.display="none";
    document.getElementById('profilemarketplacepurchases').style.display="none";
    document.getElementById('profilestatistics').style.display="none";
    document.getElementById('profilebethistory').style.display="none";
}

function listingsOpen() {
    document.getElementById('profiledeposits').style.display="none";
    document.getElementById('profilewithdraws').style.display="none";
    document.getElementById('profilemarketplacelistings').style.display="block";
    document.getElementById('profilemarketplacepurchases').style.display="none";
    document.getElementById('profilestatistics').style.display="none";
    document.getElementById('profilebethistory').style.display="none";
}

function purchasesOpen() {
    document.getElementById('profiledeposits').style.display="none";
    document.getElementById('profilewithdraws').style.display="none";
    document.getElementById('profilemarketplacelistings').style.display="none";
    document.getElementById('profilemarketplacepurchases').style.display="block";
    document.getElementById('profilestatistics').style.display="none";
    document.getElementById('profilebethistory').style.display="none";
}

function statisticsOpen() {
    document.getElementById('profiledeposits').style.display="none";
    document.getElementById('profilewithdraws').style.display="none";
    document.getElementById('profilemarketplacelistings').style.display="none";
    document.getElementById('profilemarketplacepurchases').style.display="none";
    document.getElementById('profilestatistics').style.display="block";
    document.getElementById('profilebethistory').style.display="none";
}

function openBetHistory() {
    document.getElementById('profiledeposits').style.display="none";
    document.getElementById('profilewithdraws').style.display="none";
    document.getElementById('profilemarketplacelistings').style.display="none";
    document.getElementById('profilemarketplacepurchases').style.display="none";
    document.getElementById('profilestatistics').style.display="none";
    document.getElementById('profilebethistory').style.display="block";
}