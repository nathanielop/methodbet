<?php
    require 'steamauth/steamauth.php';
    require 'steamauth/userInfo.php';

    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');    

    session_start();

    if(isset($_SESSION['steamid'])){
        $id = $_SESSION['steamid'];
        $isLoggedIn = true;
        $_SESSION['userName'] = $steamprofile['personaname'];
        $getUserDataQuery = mysqli_query($conn, "SELECT * FROM _users WHERE steam64 = '".$id."'");
        $getUserDataReturn = mysqli_fetch_assoc($getUserDataQuery);
        $userBalance = $getUserDataReturn["balance"];
        $userLevel = $getUserDataReturn["lvl"];
        $userRank = $getUserDataReturn["userRank"];
        $userBetTotal = $getUserDataReturn["betTotal"];
        $joinDate = $getUserDataReturn["joinDate"];
        $isUserBanned = $getUserDataReturn["isUserBanned"];
        $_SESSION['userBalance'] = $userBalance;
        $_SESSION['userLevel'] = $userLevel;
        $_SESSION['userRank'] = $userRank;
        $_SESSION['joinDate'] = $joinDate;
        $_SESSION['isUserBanned'] = $isUserBanned;
        $userPicture = $_SESSION['steam_avatar'];
        $userPictureFull = $_SESSION['steam_avatarfull'];
    }

    require 'finduser.php';

    $conn = mysqli_connect('localhost', 'methodical', '7azjZQ1TXQh5','methodic_betsite');   

    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Hyperbet</title>
        <link rel="stylesheet" href="stylesheet.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <script src="general.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
        <script>
            var userName = "<?=$_SESSION['userName'];?>"
            var userTag = "<?php if($userRank == 7) { echo "[Owner]";} elseif ($userRank == 3) { echo "[Mod]";} elseif ($userRank == 5) { echo "[Admin]";} else {}?>"
            var userPic = "<?=$userPicture?>"
            var userSteamID = "<?=$_SESSION['steamid']?>"
            var userBalance = "<?=$userBalance?>"
            var userbalance = "<?=$userBalance?>"
            var user64id = "<?=$_SESSION['steamid']?>"
            const socket = io('http://198.20.228.80:8002');
            socket.on('error', function(e){
                console.log('error', e);
            });
            socket.on('userBetBlack', function(listBlackBet){
                $('#betListBlack').append($('<li><img src="'+ listBlackBet.betPicLink + '"class="betListingPicture"><h3 class="bettingUserName">' + listBlackBet.betTag + ' ' + listBlackBet.betName + '</h3><h3 class="bettingAmount">' + listBlackBet.betAmtValue + '</h1>'))
            });
            socket.on('userBetGreen', function(listGreenBet){
                $('#betListGreen').append($('<li><img src="'+ listGreenBet.betPicLink + '"class="betListingPicture"><h3 class="bettingUserName">' + listGreenBet.betTag + ' ' + listGreenBet.betName + '</h3><h3 class="bettingAmount">' + listGreenBet.betAmtValue + '</h1>'));
            });
            socket.on('userBetRed', function(listRedBet){
                $('#betListRed').append($('<li><img src="'+ listRedBet.betPicLink + '"class="betListingPicture"><h3 class="bettingUserName">' + listRedBet.betTag + ' ' + listRedBet.betName + '</h3><h3 class="bettingAmount">' + listRedBet.betAmtValue + '</h1>'))
            });
        </script>
    </head>
        <?php include "header.php"?>
        <?php include "chat.php"?>
        <?php include "tos.php"?>
        <?php include "provablyfair.php"?>
        <?php include "affiliates.php"?>
        <? if(!isset($_SESSION['steamid'])) {?>
        <div id="welcomePop">
            <h1 class="mainIntro">Welcome to Hyperbet!</h1>
            <h2>What makes us so revolutionary?</h2>
            <ul class="mainFeatures">
                <li><img src="img\easyDeposit.png" alt="Ease of Deposit"></li>
                <li><img src="img\altGames.png" alt="Multiple Gamemodes"></li>
                <li><img src="img\multWithdraw.png" alt="Multiple Withdraw Methods"></li>
            </ul>
            <ul class="mainDescriptions">
                <li><h2>Fast Deposits</h2></li>
                <li><h2>Multiple Gamemodes</h2></li>
                <li><h2>Easy Withdraw</h2></li>
            </ul>
            <h1 class="lowerText">Interested?</h1>
            <div class="signUp">
                <a href="?login" class="signUpButton">Sign Up Today!</a>
            </div>          
            <button><a href="javascript:closeout()" id="closeOut">Sorry, not right now.</a></button>             
        </div>
        <? } ?>
        <div id="mainContent">
            <div id="countdownTimer"><h2>Next Round in: </h2></div>
            <div id="stdroulette-container" class="">
                <div id="roulette-container-cover-div"></div>
                <div id="roulette-container-background"></div>
                <div id="roulette-indicator-id" class="roulette-indicator"></div>
                <div id="roulette-images">
                    <div id = "roulette-images-list-list">
                    <ul id="roulette-images-list">
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                        <li class="green"><h3 class="betColorListInsideText">0</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">1</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">2</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">3</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">4</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">5</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">6</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">7</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">8</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">9</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">10</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">11</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">12</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">13</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">14</h3></li>
                        <li class="red"><h3 class="betColorListInsideText">15</h3></li>
                        <li class="black"><h3 class="betColorListInsideText">16</h3></li>
                    </ul>
                    </div>
                </div>
            </div>
            <div id="rouletteHashInformation"><h3>Round Seed: (Secret: )</h3></div>
            <div id="roulettePreviousRounds"><ul id="previousRoundsUL"></ul></div>
            <ul id="betValue">
                <li id="gameBalance"><h3>Balance: <?php if(isset($_SESSION['steamid'])) { echo "$balance1"; } else { echo "0"; }?></h3></li>
                <li><h3>Bet Amount:</h3></li>
                <li><input id="userBetAmountInNum" type="number" name="betAmount"></li>
                <li><ul id="betValueModifierBTNS">
                    <li><button id="halftimesbetBTN">1/2x</button></li>
                    <li><button id="twotimesbetBTN">2x</button></li>
                    <li><button id="maxbetBTN">All-in</button></li>
                    <li><button id="clearbetBTN">Clear</button></li>
                </ul></li>
            </ul>
            <ul id="betSelect">
                <li><button id="blackBetButton" name="blackBet">Black</button><h2>Black Total:</h2></li>
                <li><button id="greenBetButton" name="greenBet">Green</button><h2>Green Total:</h2></li>
                <li><button id="redBetButton" name="redBet">Red</button><h2>Red Total:</h2></li>
            </ul> 
            <div id="allBetList">
                <ul id="betListBlack"></ul>
                <ul id="betListGreen"></ul>
                <ul id="betListRed"></ul>
            </div> 
        </div>
    </body>
    <script type="text/javascript" src="general.js"></script>
    <script type="text/javascript" src="betroulettescript.js"></script>
</html>